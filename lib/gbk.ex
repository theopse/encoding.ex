defmodule Encoding.GBK do
  @moduledoc """
  Documentation for `Encoding`.
  """
  defstruct term: nil

  def encode(str, from \\ %Encoding{encoding: :"UTF-8"})
  def encode(str, from) when is_binary(str), do: %Encoding.GBK{term: :iconv.convert(to_string(from), "GBK", str)}
  def encode(str, from), do: to_string(str) |> encode(from)

  def new(str), do: %Encoding.GBK{term: str}


  defimpl Encoding.Protocol do
    def encode(%Encoding.GBK{term: str}, :default), do: Encoding.UTF_8.encode(str, %Encoding{encoding: :"GBK"})
    def encode(term, %Encoding{encoding: to}), do: encode(term, to)
    def encode(%Encoding.GBK{term: str}, to), do: Encoding.parse(to).encode(str, %Encoding{encoding: :"GBK"})
  end

end
