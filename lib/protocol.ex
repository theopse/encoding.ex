defprotocol Encoding.Protocol do
  def encode(item, to \\ :default)
end


defimpl Encoding.Protocol, for: String do
  def encode(item, to \\ :default), do: Encoding.encode(%Encoding.UTF_8{term: item}, to)
end

defimpl Encoding.Protocol, for: BitString do
  def encode(item, to \\ :default), do: Encoding.encode(%Encoding.UTF_8{term: item}, to)
end
