defmodule Encoding.SHIFT_JIS do
  @moduledoc """
  Documentation for `Encoding`.
  """
  defstruct term: nil

  def encode(str, from \\ %Encoding{encoding: :"UTF-8"})
  def encode(str, from) when is_binary(str), do: %Encoding.SHIFT_JIS{term: :iconv.convert(to_string(from), "Shift-JIS", str)}
  def encode(str, from), do: to_string(str) |> encode(from)

  def new(str), do: %Encoding.SHIFT_JIS{term: str}


  defimpl Encoding.Protocol do
    def encode(%Encoding.SHIFT_JIS{term: str}, :default), do: Encoding.UTF_8.encode(str, %Encoding{encoding: :"Shift-JIS"})
    def encode(term, %Encoding{encoding: to}), do: encode(term, to)
    def encode(%Encoding.SHIFT_JIS{term: str}, to), do: Encoding.parse(to).encode(str, %Encoding{encoding: :"Shift-JIS"})
  end

  #def decode(%Encoding.SHIFT_JIS{term: str}), do: :iconv.convert("Shift-JIS", "UTF-8", str)
  #def decode(str) when is_binary(str), do: :iconv.convert("Shift-JIS", "UTF-8", str)
  #def decode(str), do: to_string(str) |> decode
end
