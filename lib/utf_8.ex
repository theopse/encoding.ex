defmodule Encoding.UTF_8 do
  @moduledoc """
  Documentation for `Encoding`.
  """
  defstruct term: nil

  def encode(str, from \\ %Encoding{encoding: :"UTF-8"})
  def encode(str, from) when is_binary(str), do: %Encoding.UTF_8{term: :iconv.convert(to_string(from), "UTF-8", str)}
  def encode(str, from), do: to_string(str) |> encode(from)

  def new(str), do: %Encoding.UTF_8{term: str}

  defimpl Encoding.Protocol do
    def encode(item, :default), do: item
    def encode(term, %Encoding{encoding: to}), do: encode(term, to)
    def encode(%Encoding.UTF_8{term: str}, to), do: Encoding.parse(to).encode(str, %Encoding{encoding: :"UTF-8"})
  end

end
