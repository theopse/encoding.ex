defmodule Encoding do
  @moduledoc """
  Documentation for `Encoding`.
  """

  defstruct encoding: :"UTF-8"

  @spec new(any) :: :failed | %Encoding{encoding: :GBK | :"Shift-JIS"}
  def new(%Encoding.SHIFT_JIS{}), do: %Encoding{encoding: :"Shift-JIS"}
  def new(%Encoding.GBK{}), do: %Encoding{encoding: :"GBK"}
  def new(term) do
    code =
      term
      |> to_string()
      |> String.downcase

    case code do
      shift_jis when shift_jis in ["shift-jis", "shift_jis"] ->
        %Encoding{encoding: :"Shift-JIS"}
      gbk when gbk == "gbk" ->
        %Encoding{encoding: :"GBK"}
      _ ->
        :failed
    end
  end

  def parse(:"Shift-JIS"), do: Encoding.SHIFT_JIS
  def parse(:"GBK"), do: Encoding.GBK
  def parse(:default), do: Encoding.Default
  def parse(:failed), do: nil
  def parse(%Encoding{encoding: term}), do: parse(term)
  def parse(other), do: Encoding.new(other).encoding |> parse

  defdelegate encode(term, to \\ :default), to: Encoding.Protocol

end
