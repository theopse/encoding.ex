
list = [
  Encoding.SHIFT_JIS,
  Encoding.UTF_8,
  Encoding.GBK
]
for module <- list do
  defimpl String.Chars, for: module do
    def to_string(item), do: item.term
  end
end


defimpl String.Chars, for: Encoding do
  def to_string(%Encoding{encoding: code}), do: code |> String.Chars.to_string()
end
